# ObjectCProgramDeveloper

Object-C程序设计（第6版）源代码

第2章 Objective-C编程
    1. FirstProgram                             ===> 一个简单的示例程序
    2. UseNSLog                                 ===> 使用NSLog函数
    3. UseEscape                                ===> 使用换行转义字符
    4. ShowVariable                             ===> 显示变量的值
    5. ShowMoreVariable                         ===> 使用NSLog显示多个变量的值

第3章 类、对象和方法
    1. Fraction                                 ===> 显示分数
    2. FractionClass                            ===> 使用Fraction类表示分数
    3. UseMoreFraction                          ===> 使用多个Fraction类