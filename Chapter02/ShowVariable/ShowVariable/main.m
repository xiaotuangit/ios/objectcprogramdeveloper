//
//  main.m
//  ShowVariable
//
//  Created by QinTuanye on 2018/10/15.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int sum;
        
        sum = 50 + 25;
        NSLog(@"The sum of 50 and 25 is %i", sum);
    }
    return 0;
}
