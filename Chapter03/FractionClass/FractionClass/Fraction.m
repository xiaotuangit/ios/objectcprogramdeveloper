//
//  Fraction.m
//  Chapter03-02_FractionClass
//
//  Created by QinTuanye on 2018/10/15.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction {
    int numerator;
    int denominator;
}

- (void)print
{
    NSLog(@"%i/%i", numerator, denominator);
}

- (void)setNumerator:(int)n
{
    numerator = n;
}

- (void)setDenominator:(int)d
{
    denominator = d;
}

@end
