//
//  Fraction.h
//  Chapter03-02_FractionClass
//
//  Created by QinTuanye on 2018/10/15.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Fraction : NSObject

-(void) print;
-(void) setNumerator: (int)n;
-(void) setDenominator: (int)d;

@end

NS_ASSUME_NONNULL_END
