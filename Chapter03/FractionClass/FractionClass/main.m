//
//  main.m
//  Chapter03-02_FractionClass
//
//  Created by QinTuanye on 2018/10/15.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fraction.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Fraction *myFraction;
        
        // 创建一个分数实例
        
        myFraction = [Fraction alloc];
        myFraction = [myFraction init];
        
        // 设置分数为1/3
        
        [myFraction setNumerator:1];
        [myFraction setDenominator:3];
        
        // 使用打印方法显示分数
        
        NSLog(@"The value of myFraction is: ");
        [myFraction print];
    }
    return 0;
}
