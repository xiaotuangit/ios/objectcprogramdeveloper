Fraction

应用程序功能说明
    创建Fraction类，并使用该类表示分数

应用程序技术说明
    （1）创建类
        1. Fraction.h 头文件
            #import <Foundation/Foundation.h>

            NS_ASSUME_NONNULL_BEGIN

            @interface Fraction : NSObject

            -(void) print;
            -(void) setNumerator: (int)n;
            -(void) setDenominator: (int)d;

            @end

            NS_ASSUME_NONNULL_END

        2. Fraction.m 文件
            #import "Fraction.h"

            @implementation Fraction {
                int numerator;
                int denominator;
            }

            - (void)print
            {
                NSLog(@"%i/%i", numerator, denominator);
            }

            - (void)setNumerator:(int)n
            {
                numerator = n;
            }

            - (void)setDenominator:(int)d
            {
                denominator = d;
            }

            @end