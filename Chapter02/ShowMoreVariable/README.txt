ShowMoreVariable

应用程序功能说明
    使用NSLog显示多个变量的值

应用程序技术说明
    （1）使用NSLog显示多个变量的值：
        NSLog(@"The sum of %i and %i is %i", value1, value2, sum);