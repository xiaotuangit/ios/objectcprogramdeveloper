//
//  main.m
//  FirstProgram
//
//  Created by QinTuanye on 2018/10/15.
//  Copyright © 2018年 QinTuanye. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"Programming is fun!");
    }
    return 0;
}
